const express = require('express');
const isAuth = require('../../middleware/check-auth')
const Controller = require('./mailerController')

const router = express.Router();

router.post('/send-test-email/:id', isAuth, Controller.sendTestEmail);

router.post('/mass-sending', isAuth, Controller.massMailing);


module.exports = router; 