const express = require('express');
const Controller = require('./recipientController')
const isAuth = require('../../middleware/check-auth')

const router = express.Router();

router.get('/get-recipients', isAuth, Controller.getRecipients);

router.post('/create-new-recipient', isAuth, Controller.createNewRecipient);

router.put('/update-recipient/:id', isAuth, Controller.updateRecipient);

router.delete('/remove-recipient/:id', isAuth, Controller.removeRecipient);

module.exports = router; 