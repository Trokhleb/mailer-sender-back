const Emails = require("../../models/email");

exports.getEmailList = (req, res, next) => {
  Emails.find({ "user.userId": req.userId })
    .then((result) => {
      res.status(200).json({
        result,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.createNewEmail = (req, res, next) => {
  const email = new Emails({
    name: req.body.name,
    user: {
      userId: req.userId,
    },
    description: req.body.description,
  });
  email
    .save()
    .then((results) => {
      res.status(201).json({
        message: "Email created successfully!",
        post: {
          results,
        },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.deleteEmail = (req, res, next) => {
  const id = req.url.split("/")[2];
  Emails.findById(id)
      .then((email) => {
          return Emails.deleteOne({
              _id: id
          });
      })
      .then(() => {
          res.status(200).json({
              message: "Email deleted!",
          });
      })
      .catch((err) => {
          console.log(err);
      });
};
