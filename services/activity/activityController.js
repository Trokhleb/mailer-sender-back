const ActivityStats = require("../../models/ActivityStats");
const client = require("@sendgrid/client");
const request = require("request");

client.setApiKey(
  "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"
);

function getEndDate() {
  let dateObj = new Date();
  let month = dateObj.getUTCMonth() + 1;
  month = month < 10 ? `0${month}` : month;
  let day = dateObj.getUTCDate();
  day = day < 10 ? `0${day}` : day;
  let year = dateObj.getUTCFullYear();
  return year + "-" + month + "-" + day;
}

function getStartDate() {
  var dateObj = new Date();
  dateObj.setDate(1);
  dateObj.setMonth(dateObj.getMonth() - 1);
  let month = dateObj.getUTCMonth() + 1;
  month = month < 10 ? `0${month}` : month;
  let day = dateObj.getUTCDate();
  day = day < 10 ? `0${day}` : day;
  let year = dateObj.getUTCFullYear();
  return year + "-" + month + "-" + day;
}

exports.getStats = (req, res, next) => {
  let startDate = getStartDate();
  let endDate = getEndDate();
  const queryParams = {
    aggregated_by: "day",
    end_date: endDate,
    limit: 10,
    offset: 1,
    start_date: startDate,
  };
  request.qs = queryParams;
  request.method = "GET";
  request.url = "/v3/stats";

  client
    .request(request)
    .then(([response, body]) => {
      res.status(200).json({
        stats: response,
      });
    })
    .catch((err) => {
      console.log(err.response.body.errors);
    });
};

exports.getActivity = (req, res, next) => {
  request.method = "GET";
  request.url = "/v3/messages";
  request.qs = {
    limit: 30,
  };
  client
    .request(request)
    .then((response) => {
      res.status(200).json({
        activity: response,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.refresh = (req, res, next) => {
  ActivityStats.deleteMany({});
};
