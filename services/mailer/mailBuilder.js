const headerMailWrap = ``;
const footerMailWrap = ``;

exports.buildEmail = (emailTemplate) => {
  let htmlContent = headerMailWrap;
  htmlContent += emailTemplate.header;
  emailTemplate.body.forEach((element, index) => {
    switch (element.component) {
      case "text":
        htmlContent += `<tr>${compileTextComponent(element, index)}</tr>`;
        break;
      case "image":
        htmlContent += `<tr>${compileImageComponent(element, index)}</tr>`;
        break;
      case "button":
        htmlContent += `<tr>${compileButtonComponent(element, index)}</tr>`;
        break;
      case "link":
        htmlContent += `<tr>${compileLinkComponent(element, index)}</tr>`;
        break;
      case "video":
        htmlContent += `<tr>${compileVideoComponent(element, index)}</tr>`;
        break;
      case "divider":
        htmlContent += `<tr>${compileDividerComponent(element, index)}</tr>`;
        break;
      default:
        break;
    }
  });
  htmlContent += emailTemplate.footer;
  htmlContent += footerMailWrap;
  return htmlContent;
};

function compileTextComponent(elem, index) {
  let tdStyle = "";
  for (const [key, value] of Object.entries(elem.styleProperties)) {
    switch (key) {
      case "padding":
        tdStyle += key + paddingBuilder(value);
        break;
      case "color":
        tdStyle += `${key}:${value};`;
        break;
      case "font-family":
        tdStyle += `${key}:${value};`;
        break;
      case "background-color":
        tdStyle += `${key}:${value};`;
        break;
      case "font-style":
        tdStyle += `${key}:${value};`;
        break;
      case "text-align":
        tdStyle += `${key}:${value};`;
        break;
      case "font-weight":
        tdStyle += `${key}:${value};`;
        break;
      case "text-decoration":
        tdStyle += `${key}:${value};`;
        break;
      default:
        tdStyle += `${key}:${value}px;`;
        break;
    }
  }
  return `<td style="${tdStyle}">${elem.text}</td>`;
}

function compileImageComponent(elem, index) {
  let tdStyle = "";
  let imgStyle = "";
  for (const [key, value] of Object.entries(elem.styleProperties)) {
    switch (key) {
      case "padding":
        tdStyle += key + paddingBuilder(value);
        break;
      case "background-color":
        tdStyle += `${key}:${value};`;
        break;
      default:
        tdStyle += `${key}:${value}px;`;
        break;
    }
  }
  for (const [key, value] of Object.entries(elem.elementsProperties)) {
    imgStyle += ` ${key}="${value}" `;
  }
  return `<td align="center" style="${tdStyle}"><img ${imgStyle} style="display:block"/></td>`;
}

function compileButtonComponent(elem, index) {
  let aStyle = "";
  let aComp = "";
  let tdStyle = "";
  let tdComp = "";
  let tdPadd = "";
  for (const [key, value] of Object.entries(elem.styleProperties2)) {
    switch (key) {
      case "padding":
        tdStyle += key + paddingBuilder(value);
        break;
      case "background-color":
        tdStyle += `${key}:${value};`;
        break;
      default:
        tdStyle += `${key}:${value}px;`;
        break;
    }
  }
  for (const [key, value] of Object.entries(elem.outerProperties)) {
    tdComp += ` ${key}="${value}" `;
  }
  for (const [key, value] of Object.entries(elem.styleProperties)) {
    switch (key) {
      case "color":
        aStyle += `${key}:${value} !important;`;
        break;
      case "font-family":
        aStyle += `${key}:${value};`;
        break;
      case "font-style":
        aStyle += `${key}:${value};`;
        break;
      case "text-align":
        aStyle += `${key}:${value};`;
        break;
      case "font-weight":
        aStyle += `${key}:${value};`;
        break;
      case "text-decoration":
        aStyle += `${key}:${value};`;
        break;
      default:
        aStyle += `${key}:${value}px;`;
        break;
    }
  }
  for (const [key, value] of Object.entries(elem.elementsProperties)) {
    aComp += ` ${key}="${value}" `;
  }
  for (const [key, value] of Object.entries(elem.outerProperties2)) {
    switch (key) {
      case "padding":
        tdPadd += key + paddingBuilder(value);
        break;
      default:
        tdPadd += `${key}:${value}px;`;
        break;
    }
  }
  return `<td ${tdComp} style="${tdPadd}"><table border="0" cellpadding="0" cellspacing="0"><tr><td style="${tdStyle}"><a style="${aStyle}" ${aComp}>${elem.text}</a></td></tr></table></td>`;
}

function compileLinkComponent(elem, index) {
  let tdStyle = "";
  let aComp = "";
  for (const [key, value] of Object.entries(elem.styleProperties)) {
    switch (key) {
      case "padding":
        tdStyle += key + paddingBuilder(value);
        break;
      case "color":
        tdStyle += `${key}:${value};`;
        break;
      case "font-family":
        tdStyle += `${key}:${value};`;
        break;
      case "background-color":
        tdStyle += `${key}:${value};`;
        break;
      case "font-style":
        tdStyle += `${key}:${value};`;
        break;
      case "text-align":
        tdStyle += `${key}:${value};`;
        break;
      case "font-weight":
        tdStyle += `${key}:${value};`;
        break;
      case "text-decoration":
        tdStyle += `${key}:${value};`;
        break;
      default:
        tdStyle += `${key}:${value}px;`;
        break;
    }
  }
  for (const [key, value] of Object.entries(elem.elementsProperties)) {
    aComp += ` ${key}="${value}" `;
  }
  return `<td style="${tdStyle}"><a ${aComp} style="color:${elem.styleProperties.color};text-decoration:${elem.styleProperties["text-decoration"]};">${elem.text}</a></td>`;
}

function compileVideoComponent(elem, index) {
  let tdStyle = "";
  let imgStyle = "";
  let aComp = "";
  for (const [key, value] of Object.entries(elem.styleProperties)) {
    switch (key) {
      case "padding":
        tdStyle += key + paddingBuilder(value);
        break;
      case "background-color":
        tdStyle += `${key}:${value};`;
        break;
      default:
        tdStyle += `${key}:${value}px;`;
        break;
    }
  }
  for (const [key, value] of Object.entries(elem.elementsProperties)) {
    imgStyle += ` ${key}="${value}" `;
  }
  for (const [key, value] of Object.entries(elem.elementsProperties2)) {
    aComp += ` ${key}="${value}" `;
  }
  return `<td align="center" style="${tdStyle}"><a ${aComp} ><img ${imgStyle}style="display:block"/></a></td>`;
}

function compileDividerComponent(elem, index) {
  let tdStyle = "";
  for (const [key, value] of Object.entries(elem.styleProperties)) {
    switch (key) {
      case "padding":
        tdStyle += key + paddingBuilder(value);
        break;
      case "background-color":
        tdStyle += `${key}:${value};`;
        break;
      default:
        tdStyle += `${key}:${value}px;`;
        break;
    }
  }
  return `<td style="${tdStyle}"></td>`;
}

function paddingBuilder(value) {
  let padding = ":";
  for (const [key, paddingSide] of Object.entries(value)) {
    padding += paddingSide + "px ";
  }
  padding += ";";
  return padding;
}
