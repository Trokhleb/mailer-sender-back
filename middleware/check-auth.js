const jwt = require('jsonwebtoken');
const config = require('../configs/config');

module.exports = (req, res, next) => {
    const authHeader = req.headers.authorization;
    if(!authHeader) {
      const error = new Error('Not authenticated.')
      error.statusCode = 401;
      throw error;
    }
    let decodedToken;
    try {
      decodedToken = jwt.verify(authHeader, config.secret)
    } catch (err) {
      err.statusCode = 500;
      throw err;
    }
    if(!decodedToken) {
      const error = new Error('Not authenticated.');
      err.statusCode = 401;
      throw error; 
    }

    req.userId = decodedToken.userId;
    next();
  }
  