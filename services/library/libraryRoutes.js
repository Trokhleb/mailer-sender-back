const express = require('express');
const Controller = require('./libraryController')
const isAuth = require('../../middleware/check-auth')

const router = express.Router();

router.post('/create-new-email', isAuth, Controller.createNewEmail);

router.get('/get-email-list', isAuth, Controller.getEmailList);

router.delete('/remove-email/:id', isAuth, Controller.deleteEmail);

module.exports = router; 