const builder = require("./mailBuilder");
const config = require("../../configs/config");
const User = require("../../models/user");

exports.sendTestEmail = (req, res, next) => {
  const buildedEmail = builder.buildEmail(req.body.email.emailTemplate);
  const sgMail = require("@sendgrid/mail");
  sgMail.setApiKey('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');
  const id = req.userId;
  User.findOne({ _id: id })
  .then((user) => {
    const msg = {
      to: user.email,
      from: "sendify.mailer@gmail.com",
      subject: `Send a test of your email ${req.body.email.name}`,
      text: "Test easy to do anywhere, even with Node.js",
      html: buildedEmail,
    };
    sgMail
      .send(msg)
      .then(() => {
        console.log("Email sent");
      })
      .catch((error) => {
        console.error(error);
      });
  })
};

exports.massMailing = (req, res, next) => {
  const sgMail = require("@sendgrid/mail");
  sgMail.setApiKey('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');
  const buildedEmail = builder.buildEmail(req.body.email);
  const recipients = req.body.recipients;
  const subject = req.body.subject;
  recipients.forEach(recipient => {
    let msg = {
      to: recipient.email,
      from: "sendify.mailer@gmail.com",
      subject: subject,
      text: "Sendify mass mailing tool",
      html: buildedEmail,
    };
    sgMail
      .send(msg)
      .then(() => {
        console.log("Email sent");
      })
      .catch((error) => {
        console.error(error);
      });
  })
};
