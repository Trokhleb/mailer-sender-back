const express = require('express');
const Controller = require('./editorController')
const isAuth = require('../../middleware/check-auth')

const router = express.Router();

router.get('/get-email/:id', isAuth, Controller.getEmail);

router.get('/view-email/:id', isAuth, Controller.viewEmail);

router.put('/save-email/:id', isAuth, Controller.saveEmail);


module.exports = router; 