const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  user: {
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
  },
  description: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: "Draft",
  },
  emailTemplate: {
    type: Object,
    default: {
      header: `<table align="center" border="0" cellpadding="0" cellspacing="0" width="700">`,
      footer: `</table>`,
      body: [
        {
          component: "text",
          text:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.",
          styleProperties: {
            padding: {
              top: 20,
              right: 30,
              bottom: 20,
              left: 30,
            },
            color: "#153643",
            "font-family": "Arial",
            "font-size": 17,
            "line-height": 20,
            "text-align": "left",
            "background-color": "#ffffff",
            "font-style": "normal",
          },
        },
      ],
    },
  },
});

module.exports = mongoose.model("Email", userSchema);
