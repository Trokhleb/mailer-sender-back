const express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  path = require("path"),
  mongoose = require("mongoose"),
  config = require("./configs/config"),
  helmet = require("helmet");

// DB Models
const User = require('./models/user');

// Routes
const authRoutes = require('./services/users/usersRoutes');
const libraryRoutes = require('./services/library/libraryRoutes');
const editorRoutes = require('./services/editor/editorRoutes');
const mailerRoutes = require('./services/mailer/mailerRoutes');
const recipientRoutes = require('./services/recipient/recipientRoutes');
const activityRoutes = require('./services/activity/activityRoutes');

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "public")));

// API
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "OPTIONS, GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

// Use routes
app.use('/auth', authRoutes);
app.use('/library', libraryRoutes);
app.use('/editor', editorRoutes);
app.use('/mailer', mailerRoutes);
app.use('/recipient', recipientRoutes);
app.use('/activity', activityRoutes);

// ERROR Handling
app.use((error, req, res, next) => {
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ message: message, data: data });
});

// 404
app.use((req, res, next) => {
  res.status(404).json({ message: 'Not Found'});
});

//Server and DB connection
mongoose
  .connect(config.mongodb_URL, { useNewUrlParser: true, useUnifiedTopology: true, retryWrites:false})
  .then((result) => {
    app.listen(config.port, () => {
      User.findOne().then((user) => {
        if (!user) {
          const user = new User({
            username: "admin-96001",
            firstName: "admin",
            lastName: "admin",
            companyPhone: "+380118883344",
            companyAddress: "Ukraine, Zhytomyr, Pushkinskaya 2, 53",
            email: "test@test.com",
            password: "zaraki228",
          });
          user.save();
        }
      });
      console.log("Server start work. Mongoose connected to the db.");
    });
  })
  .catch((err) => {
    console.log(err);
  });
