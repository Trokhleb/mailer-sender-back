const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../../configs/config");
const User = require("../../models/user");

exports.signup = (req, res, next) => {
  bcrypt
    .hash(req.body.password, 10)
    .then((hash) => {
      const user = new User({
        username: req.body.username,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        companyPhone: req.body.companyPhone,
        companyAddress: req.body.companyAddress,
        password: hash,
      });
      return user.save();
    })
    .then((result) => {
      res.status(201).json({
        message: "User created!",
        user: result,
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.login = (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  User.findOne({ username: username })
    .then((user) => {
      if (!user) {
        res.status(200).json({
          code: 404,
          message: "User not found",
        });
      }
      authUser = user;
      return bcrypt.compare(password, user.password);
    })
    .then((isEqual) => {
      if (!isEqual) {
        res.status(200).json({
          code: 404,
          message: "Username and passowrd does not match",
        });
      }
      const token = jwt.sign(
        {
          email: authUser.email,
          userId: authUser._id.toString(),
        },
        config.secret,
        { expiresIn: "24h" }
      );
      res.status(200).json({
        token: token,
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
