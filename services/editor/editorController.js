const e = require("express");
const Emails = require("../../models/email");

exports.getEmail = (req, res, next) => {
  const id = req.url.split("/")[2];
  Emails.findById(id)
    .then((result) => {
      res.status(200).json({
        email: result,
      }); 
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.viewEmail = (req, res, next) => {
  const id = req.url.split("/")[2];
  Emails.findById(id)
    .then((result) => {
      res.status(200).json({
        email: result,
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

exports.saveEmail = (req, res, next) => {
  const emailSave = {
    id: req.body.id,
    emailTemplate: req.body.emailTemplate,
    name: req.body.name
  };
  Emails.findById(emailSave.id)
    .then((email) => {
      email.emailTemplate = emailSave.emailTemplate;
      email.name = emailSave.name;
      return email.save().then((result) => {
        res.status(201).json({
          message: "Email updated",
        });
      });
    })
    .catch((err) => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};
