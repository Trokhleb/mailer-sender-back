const Recipient = require("../../models/recipient");

exports.getRecipients = (req, res, next) => {
    Recipient.find({
            "user.userId": req.userId
        })
        .then((result) => {
            res.status(200).json({
                result,
            });
        })
        .catch((err) => {
            console.log(err);
        });
};

exports.createNewRecipient = (req, res, next) => {
    const recipient = req.body.recipient;
    recipient.user = {
        userId: req.userId
    };
    const recipientNew = new Recipient(recipient);
    recipientNew
        .save()
        .then((results) => {
            res.status(201).json({
                message: "Recipient created successfully!",
                post: {
                    results,
                },
            });
        })
        .catch((err) => {
            console.log(err);
        });
};

exports.updateRecipient = (req, res, next) => {
    const id = req.url.split("/")[2];
    const update = req.body.recipient;
    Recipient.findById(id)
        .then((recipient) => {
            recipient.firstName = update.firstName;
            recipient.lastName = update.lastName;
            recipient.email = update.email;
            recipient.phone = update.phone;
            recipient.salutation = update.salutation;
            recipient.title = update.title;
            return recipient.save().then((result) => {
                res.status(201).json({
                    message: "Recipient updated",
                });
            });
        })
        .catch((err) => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

exports.removeRecipient = (req, res, next) => {
    const id = req.url.split("/")[2];
    Recipient.findById(id)
        .then((recipient) => {
            return Recipient.deleteOne({
                _id: id
            });
        })
        .then(() => {
            res.status(200).json({
                message: "Recipient deleted!",
            });
        })
        .catch((err) => {
            console.log(err);
        });
};