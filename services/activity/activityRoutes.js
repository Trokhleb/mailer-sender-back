const express = require('express');
const Controller = require('./activityController')
const isAuth = require('../../middleware/check-auth')

const router = express.Router();

router.get('/get-stats', isAuth, Controller.getStats);

router.get('/get-activity', isAuth, Controller.getActivity);

router.get('/refresh', isAuth, Controller.refresh)


module.exports = router; 