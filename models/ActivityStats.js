const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const activityStatsSchema = new Schema({
    activity: {
        type: Array,
    },
    stats: {
        type: Array
    }
});

module.exports = mongoose.model("ActivityStats", activityStatsSchema);
